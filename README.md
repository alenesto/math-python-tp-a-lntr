# Git en pratique pour l'industrie

## Partie 1 : Commandes avancées Git

1. Le projet  a été cloné à partir d'un fork du projet de base avec la commande ```git clone```.

2. A l'aide de la commande ```python3 -m pytest -v tests```, les tests sont lancés et tous ne passent pas. dans le fichiers ```test_functions.py```, seuls 88% des tests sont passés.

3. Grâce à ```git branch nom-branche``` (fix-function-fibonacci), je crée une nouvelle branche, puis avec ```git checkout nom-branche```, je change de branche.

4. Je réalise un premier commit (```git add``` et ```git commit```) pour corriger le nom de la fonction de test de Fibonacci puis modifie le code de la fonction de Fibonnacci en elle-même. En effet, après avoir effectué la première  modification, j'ai à nouveau lancé les tests et la fonction ne fonctionne pas. Il faut donc procéder à une deuxième modification.

J'ai eu besoin d'ajouter un .gitignore pour ignorer un certain nombre de fichiers lors de mes commits (dossiers .idea et __pycache__)

5. J'ai ensuite lancé la commande ```git rebase -i``` pour réoganiser mes commits qui pouvaient être fusionnés. J'ai donc remplacé ```pick```par ```squash```pour mes deux derniers commits (modification de la fonction de fibonacci et l'ajout du fichier .gitignore) et j'ai fait un commit en plusieurs lignes pour expliquer le contenu de tous mes commits précédents.

6. J'ai ensuite lancé la commande ```git bisect start id-commit-mauvais id-commit-bon``` pour identifier le commit de bug. Par élimination progressive, et en déterminant à l'aide de  ```git bisect good``` et ```git bisect bad``` si le commit contient une erreur ou non (en lançant les tests au fur et à mesure), j'ai pu me rendre compte que le problème venait du commit 'Replace i index with more explicit loop_count'.

7. Pour trouver qui est l'auteur des lignes de code problématiques avec ```git blame math2.functions.py```. L'auteur est donc Guillaume BERNARD.

8. En faisant ```git cherry-pick id-commit```, les modifications sont appliquées au bon endroit

__Important__
Il est indispensable de ````push``` les modifcations apportées (à noter que j'ai ```push``` sur la branche ```main``` mais que j'aurai dû push sur ma branche et ```merge``` avec ```main```)

9. ```git merge main```

10. Avec ```git format-patch id-commit```, je crée un fichier de patch dans lequel figure toutes les informations de modification (date, auteur, message de commits, modifications apportées aux fichiers)

## Partie 2 : Collaboration de code avec GitLab

1. Je travaille en binôme avec Antoine SUEUR

2. Je suis ajouté en *developer* du projet pour que je puisse travailler avec lui.

3. Antoine implémente la fonction anagramme et j'implémente la fonction crible-eratosthene sur nos branches respectives.

4. Nous faisons un commit explicite et un push de nos modifications repsectives.

5. Chacun d'entre nous procède à une merge request en précisant les branches cibles. Cependant, il n'est pas possible de merge car le code rencontre un conflit.

6. Le code est revu par chaque membre du binôme

7. Un fichier CONTRIBUTING.md est rédigé pour établir certaines règles de développement telles que le nommage des fonctions

8. Les bugs ont été corrigés à même GitLab pour prendre en compte les deux modifications.

9. Le code est commit et push.

## Partie 3 : Utilisation de pre-commit pour Python

1. ```python3 -m pip install pre-commit```

2. ajout du fichier .pre-commit-config.yaml avec les informations de repo

3. renommage de fonction pour échec du test

4. ```pre-commit install```

```pre-commit run --all-files```

5. renommage de fonction pour réussite du test

6. ```git push```
